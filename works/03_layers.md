#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

	[root@j30 ~]# systemctl stop NetworkManager
	[root@j30 ~]# dhclient -r
	[root@j30 ~]# killall dhclient
	[root@j30 ~]# ip a f dev enp3s0	
	
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	[root@j30 ~]# ip a a 2.2.2.2/24 dev enp3s0
	[root@j30 ~]# ip a a 3.3.3.3/16 dev enp3s0
	[root@j30 ~]# ip a a 4.4.4.4/25 dev enp3s0


Consulta la tabla de rutas de tu equipo

	[root@j30 ~]# ip a
	
Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	[root@j30 ~]# ping 2.2.2.2
	PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.
	64 bytes from 2.2.2.2: icmp_seq=1 ttl=64 time=0.032 ms
	
	Contesta dado que me estoi haciendo un ping a mi mismo
	
	[root@j30 ~]# ping 2.2.2.254
	PING 2.2.2.254 (2.2.2.254) 56(84) bytes of data.
	From 2.2.2.2 icmp_seq=1 Destination Host Unreachable

	Aunque la dentro del rango de la mascara el ordenador no tiene una ruta para llegar dado que en la red no hay ningun ordenador con esa ip

	[root@j30 ~]# ping 2.2.5.2
    PING 2.2.5.2 (2.2.5.2) 56(84) bytes of data.
    
    Al ser una mac /24 solo puede variar el ultimo campo P.E. 2.2.2.x 

	[root@j30 ~]# ping 3.3.3.35
	PING 3.3.3.35 (3.3.3.35) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	
	Aunque la ip esta dentro del rango de la mascara el ordenador no tiene una ruta para llegar dado que en la red no hay ningun ordenador con esa ip

	[root@j30 ~]# ping 3.3.200.45
	PING 3.3.200.45 (3.3.200.45) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	
	Aunque la ip esta dentro del rango de la mascara el ordenador no tiene una ruta para llegar dado que en la red no hay ningun ordenador con esa ip

	[root@j30 ~]# ping 4.4.4.8
	PING 4.4.4.8 (4.4.4.8) 56(84) bytes of data.
	From 4.4.4.4 icmp_seq=1 Destination Host Unreachable

    Aunque la ip esta dentro del rango de la mascara el ordenador no tiene una ruta para llegar dado que en la red no hay ningun ordenador con esa ip

	[root@j30 ~]# ping 4.4.4.132
	PING 4.4.4.132 (4.4.4.132) 56(84) bytes of data.
	
	Por que la mascara /25 ??



	
#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	[root@j30 ~]# ip a f dev enp3s0	

Conecta una segunda interfaz de red por el puerto usb

Cambiale el nombre a usb0
	
Modifica la dirección MAC

	[root@j30 ~]# ip link set enp0s26u1u2 down
	[root@j30 ~]# ip link set enp0s26u1u2 address 00:11:22:33:44:55
	[root@j30 ~]# ip link set enp0s26u1u2 name usb0
	[root@j30 ~]# ip link set usb0 up

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	[root@j30 ~]# ip a a 5.5.5.5/24 dev usb0
	[root@j30 ~]# ip a a 7.7.7.7/24 dev enp3s0

Observa la tabla de rutas

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	[root@j30 ~]# ip a f dev enp3s0
	[root@j30 ~]# ip a f dev usb0		

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	[root@j30 ~]# ip a a 172.16.99.30/24 dev enp3s0
	
Lanzar iperf en modo servidor en cada ordenador

	[root@j30 ~]# iperf -s

Comprueba con netstat en qué puerto escucha

tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      3379/iperf   

Conectarse desde otro pc como cliente

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

