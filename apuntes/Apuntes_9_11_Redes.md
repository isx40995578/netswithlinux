            
              /\
             /1 \ ------------> Fisica
            /----\
           /  2   \ ----------> Enlace
          /--------\ 
         /    3     \ --------> Red
        /------------\ 
       /      4       \ ------> Transporte
      /----------------\
     /        7         \ ----> Aplicacion
    /--------------------\
  
  IEEE = IE^3 -> ingenieros
  ethernet    -> empresas
 
 # Fisica
***
 
'''
	 
				   -Cable -> 100mb
				  /  
	- Tipo medio /_ wifi            _  multimodo -> 150m
				 \                 /
				  \- Fibra Optica <			
								   \_  monomodo -KM



	- Velocidad -> 10Mbps
				   100Mbps
				   1Gbps
				   10Gbps
				   40Gbps
				   100Gbps	

'''              
10 = 10Mbps				
10 - Base - T ( T -> twisted pair = por trenzas = 8 cables de cobre = conector RJ45 )

Fast ethernet = 100 - base - SX	 ( SX = fibra optica )	

'''

                                            _ cable cobre
                                           /
    IEEE802.3 -> capa fisicapara ethernet < 
										   \_ Fibra
																_ 2,4 GHz -> frecuencia de radio
															   / 
	IEEE802.11 -> capa fisica wifi ( telefonos inalambricos ) <	
															   \_ 5GHz

'''													    			
***

Transporte 

- Lleva control sobre si he recibido los datos y el orden en que lleguen
- Establece un flujo(velocidad) estable entre emisor y receptor
- Mecanismos para establecer una conexion "Virtual" entre emisor y receptor con mecanismos 
	* No perder datos -> retransmitir -> TCP -> Orientado a conexion
	* Asumir que puedo peder alguno -> UDP
	
'''

                 _ TCP
                /
     Transporte<-- UDP
                \_ ICMP		                

'''
